// #include "stdafx.h"
#include <vector>
#include <iostream>
#include <algorithm>
#include <limits.h>

using std::vector;

bool comp(int argOne, int argTwo)
{
  return argOne > argTwo;
}

int main()
{
  int el_number, length = 0;
  std::cin >> el_number;
  vector<int> Elements(el_number);
  vector<int> Curr_el(el_number, INT_MIN);
  vector<int> Pos(el_number, -1);
  vector<int> Prev(el_number, -1);
  vector<int> Res;
  Curr_el[0] = INT_MAX;


  for (int i = 0; i < el_number; ++i)
  {
    std::cin >> Elements[i];
    int j_elem = upper_bound(Curr_el.begin(), Curr_el.end(), Elements[i], comp) - Curr_el.begin();
    if (Curr_el[j_elem - 1] >= Elements[i] && Elements[i] > Curr_el[j_elem]) 
    { 
      Curr_el[j_elem] = Elements[i];
      Pos[j_elem] = i;
      Prev[i] = Pos[j_elem - 1];
      length = length > j_elem ? length : j_elem;
    }
  }

  std::cout << length << std::endl;
  int curr = Pos[length];
  while (curr != -1)
  {
    Res.push_back(curr + 1);
    curr = Prev[curr];
  }

  for (int i = length - 1; i >= 0; --i)
  {
    std::cout << Res[i] << " ";
  }

  return 0;
}
